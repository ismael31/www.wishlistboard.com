const { i18n } = require('./next-i18next.config');

module.exports = {
  i18n,
  env: {
    api: 'http://localhost:1337'
  },
  images: {
    domains: ['localhost']
  }
}
