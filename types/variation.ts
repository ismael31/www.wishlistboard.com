export type Variation = 'Primary' | 'Secondary' | 'Tertiary' | 'Link' | 'Lightbox';

export const BUTTON_VARIATIONS: Record<Variation, string> = {
    Primary: 'bg-carnation-500 hover:bg-carnation-700 text-white shadow-sm' +
        ' focus:ring-carnation-500 focus:ring-4 focus:ring-opacity-25',
    Secondary: 'bg-gray-200 hover:bg-gray-300 text-gray-800 hover:text-gray-900' +
        ' shadow-sm focus:ring-gray-300 focus:ring-4 focus:ring-opacity-25',
    Tertiary: 'text-carnation-500 hover:text-gray-900 ' +
        ' focus:text-gray-800 hover:bg-gray-300' +
        ' focus:bg-gray-200 focus:ring-gray-300 focus:ring-4 focus:ring-opacity-25',
    Link: 'text-carnation-500 hover:text-carnation-700' +
        ' focus:text-carnation-700 focus:ring-2' +
        ' focus:ring-carnation-500 hover:underline focus:underline focus:ring-0',
    Lightbox: 'bg-gray-900 text-gray-300 bg-opacity-60 hover:bg-opacity-80' +
        ' focus:bg-opacity-80 active:bg-opacity-80 focus:ring-0'
};
