export type Size = 'Xs' | 'Sm' | 'Md' | 'Lg' | 'Xl';

export const BUTTON_SIZES: Record<Size, string> = {
    Xs: '',
    Sm: 'px-2.5 py-1.5 text-xs',
    Md: 'px-4 py-2 text-base',
    Lg: 'px-6 py-2.5 text-lg',
    Xl: ''
};
