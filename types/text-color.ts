export type TextColor = 'Black' | 'Gray' | 'Carnation' | 'CarnationDark';

export const TEXT_COLORS: Record<TextColor, string> = {
    Black: 'text-black',
    Gray: 'text-gray-500',
    Carnation: 'text-carnation-500',
    CarnationDark: 'text-carnation-900',
};
