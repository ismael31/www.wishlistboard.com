import { Size } from "./size";

export const PADDINGS: Record<Size, string> = {
    Xs: 'p-2',
    Sm: 'p-4',
    Md: 'p-6',
    Lg: 'p-8',
    Xl: 'p-10',
}
