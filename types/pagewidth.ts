export type Pagewidth = 'Full' | 'Login' | 'Text' | 'Default';

export const PAGEWIDTHS: Record<Pagewidth, string> = {
    Full: '',
    Login: 'max-w-sm',
    Text: 'max-w-xl',
    Default: 'max-w-screen-lg',
}
