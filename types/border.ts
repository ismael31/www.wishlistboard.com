export type Border = 'None' | 'Gray';

export const BORDERS: Record<Border, string> = {
    None: '',
    Gray: 'border border-gray-200',
}
