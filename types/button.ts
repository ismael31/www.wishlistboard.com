import { Variation } from './variation';


export type ButtonType = {
    label: string;
    variation: Variation;
    url: string;
}
