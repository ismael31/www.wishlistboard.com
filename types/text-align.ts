export type TextAlign = 'None' | 'Center' | 'Left' | 'Right';

export const TEXT_ALIGNS: Record<TextAlign, string> = {
    None: '',
    Center: 'text-center',
    Left: 'text-left',
    Right: 'text-right'
}
