export type BackgroundColor = 'Transparent' | 'White' | 'Gray' | 'Carnation' | 'Bordered';

export const BACKGROUND_COLORS: Record<BackgroundColor, string> = {
    Transparent: 'bg-transparent',
    White: 'bg-white',
    Gray: 'bg-gray-100',
    Carnation: 'bg-carnation-200',
    Bordered: 'bg-transparent border'
};
