import { useRouter } from 'next/router';
import React from 'react';
import '../i18n';
import { CallToAction, CallToActionRow } from '../components';


export default function Slug() {
    const router = useRouter();

    const slug = router.query?.slug;

    return (<>

        {/*for space top*/ }
        <div></div>

        <CallToActionRow>
            <CallToAction title="pages:redirect.call-to-action.title"
                          text="pages:redirect.call-to-action.text" button={ {
                label: 'pages:redirect.call-to-action.button.label',
                href: `https://app.wishlistboard.com/${ slug }`
            } } box={ {
                backgroundColor: 'Gray'
            } }/>
        </CallToActionRow>

    </>);
}
