import Document, { DocumentContext, Head, Html, Main, NextScript } from 'next/document';


export default class MyDocument extends Document {
    static async getInitialProps(ctx: DocumentContext) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html lang="de,en">
                <Head>
                    <meta charSet="utf-8"/>
                    <meta name="author" content="Raphael Niederer"/>
                    <meta name='keywords' lang='de'
                          content="Wunschliste, Wünsche, Hochzeit, Geburtstag, Babyparty, Weihnachten, Kommunion, Firmung, Taufe, Passwort-Schutz, Export, Dankesschreiben"/>
                    <meta name='keywords' lang='en'
                          content="Wishlist, Wish, Wedding, Birthdays, Baby shower, Christmas, Communion, Confirmation, Baptism, Passwort protection, Export, Thank you letter"/>
                    <meta name='robots' content="index, follow"/>

                    <meta name="p:domain_verify" content="233c9f6175b7b825e61cf0f89360b92a"/>


                    <link rel="icon" href="/favicon.ico"/>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>

                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin=""/>
                <link
                    href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto+Slab:wght@500&display=swap"
                    rel="stylesheet"/>
            </Html>
        )
    }
}


