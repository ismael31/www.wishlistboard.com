import { useRouter } from 'next/router';
import {
    Button,
    FeatureTeaser,
    FeatureTeaserImage,
    FeatureTeaserRow,
    PageHeader,
    Paragraph,
    Section
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';


export default function Parts() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:parts.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:parts.page.header.description') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:parts.page.header.title"
                subTitle="features:parts.page.header.sub-title"
                text="features:parts.page.header.text"
            />

            <FeatureTeaserImage title="features:parts.page.feature-teaser-image.title"
                                text="features:parts.page.feature-teaser-image.text" image={ {
                alternativeText: 'features:parts.page.feature-teaser-image.image.alt',
                formats: {
                    thumbnail: {
                        url: '/images/fulfill-part-256x192.png', width: 256,
                        height: 192
                    },
                    small: {
                        url: '/images/fulfill-part-512x384.png', width: 512,
                        height: 384
                    },
                    medium: {
                        url: '/images/fulfill-part-1024x768.png', width: 1024,
                        height: 768
                    },
                    full: {
                        url: '/images/fulfill-part-2048x1536.png', width: 2048,
                        height: 1536
                    }
                }
            } } button={ {
                label: 'pages:home.call-to-action.button.label',
                href: 'http://app.wishlistboard.com/register'
            } }/>

            <Paragraph text="features:parts.page.paragraph.text"/>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="GiftIcon"
                    title="features:parts.page.feature-teaser-row.simple.title"
                    description="features:parts.page.feature-teaser-row.simple.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="CurrencyDollarIcon"
                    title="features:parts.page.feature-teaser-row.free-amount.title"
                    description="features:parts.page.feature-teaser-row.free-amount.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="ChartPieIcon"
                    title="features:parts.page.feature-teaser-row.parts.title"
                    description="features:parts.page.feature-teaser-row.parts.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="QuestionMarkCircleIcon"
                    title="features:parts.page.feature-teaser-row.payment.title"
                    description="features:parts.page.feature-teaser-row.payment.description"
                    backgroundColor="Carnation"/>
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
