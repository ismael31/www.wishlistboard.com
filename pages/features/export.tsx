import { useRouter } from 'next/router';
import {
    Button, FeatureTeaser,
    FeatureTeaserImage,
    FeatureTeaserRow,
    PageHeader,
    Paragraph,
    Section
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';


export default function Export() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:csv.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:csv.page.header.sub-title') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:csv.page.header.title"
                subTitle="features:csv.page.header.sub-title"
                text="features:csv.page.header.text"
            />

            <FeatureTeaserImage title="features:csv.page.feature-teaser-image.title"
                                text="features:csv.page.feature-teaser-image.text" image={ {
                alternativeText: 'features:csv.page.feature-teaser-image.image.alt',
                formats: {
                    thumbnail: {
                        url: '/images/donates-256x258.png', width: 256, height: 258
                    },
                    small: {
                        url: '/images/donates-512x516.png', width: 512, height: 516
                    },
                    medium: {
                        url: '/images/donates-1024x1032.png', width: 1024, height: 1032
                    },
                    full: {
                        url: '/images/donates-2048x2064.png', width: 2048, height: 2064
                    }
                }
            } } button={ {
                label: 'pages:home.call-to-action.button.label',
                href: 'http://app.wishlistboard.com/register'
            } }/>

            <Paragraph text="features:csv.page.paragraph.text"/>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="DocumentTextIcon"
                    title="features:csv.page.feature-teaser-row.thank-you-letter.title"
                    description="features:csv.page.feature-teaser-row.thank-you-letter.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="CurrencyDollarIcon"
                    title="features:csv.page.feature-teaser-row.allowance.title"
                    description="features:csv.page.feature-teaser-row.allowance.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
