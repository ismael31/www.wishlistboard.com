import { useRouter } from 'next/router';
import {
    Button,
    FeatureTeaserImage,
    Section,
    PageHeader
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';

export default function Independent() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:independent.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:independent.page.header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:independent.page.header.title"
                subTitle="features:independent.page.header.sub-title"
                text="features:independent.page.header.text"
            />

            <FeatureTeaserImage title="features:independent.page.feature-teaser-image.title"
                                text="features:independent.page.feature-teaser-image.text"
                                image={ {
                                    alternativeText: 'features:independent.page.feature-teaser-image.image.alt',
                                    formats: {
                                        thumbnail: {
                                            url: '/images/create-wish-256x192.png', width: 256,
                                            height: 192
                                        },
                                        small: {
                                            url: '/images/create-wish-512x384.png', width: 512,
                                            height: 384
                                        },
                                        medium: {
                                            url: '/images/create-wish-1024x768.png', width: 1024,
                                            height: 768
                                        },
                                        full: {
                                            url: '/images/create-wish-2048x1536.png', width: 2048,
                                            height: 1536
                                        }
                                    }
                                } } button={ {
                label: 'pages:home.call-to-action.button.label',
                href: 'http://app.wishlistboard.com/register'
            } }/>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
