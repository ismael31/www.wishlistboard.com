import { useRouter } from 'next/router';
import {
    Button,
    FeatureTeaser,
    FeatureTeaserImage,
    FeatureTeaserRow,
    PageHeader,
    Paragraph,
    Section,
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';

export default function PasswordProtection() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:password-protection.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:password-protection.page.header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:password-protection.page.header.title"
                subTitle="features:password-protection.page.header.sub-title"
                text="features:password-protection.page.header.text"
            />


            <FeatureTeaserImage title="features:password-protection.page.feature-teaser-image.title"
                                text="features:password-protection.page.feature-teaser-image.text"
                                image={ {
                                    alternativeText: 'features:password-protection.page.feature-teaser-image.image.alt',
                                    formats: {
                                        thumbnail: {
                                            url: '/images/password-protection-256x192.png', width: 256,
                                            height: 192
                                        },
                                        small: {
                                            url: '/images/password-protection-512x384.png', width: 512,
                                            height: 384
                                        },
                                        medium: {
                                            url: '/images/password-protection-1024x768.png', width: 1024,
                                            height: 768
                                        },
                                        full: {
                                            url: '/images/password-protection-2048x1536.png', width: 2048,
                                            height: 1536
                                        }
                                    }
                                } } button={ {
                label: 'pages:home.call-to-action.button.label',
                href: 'http://app.wishlistboard.com/register'
            } }/>

            <Paragraph text="features:password-protection.page.paragraph.text"/>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="LockClosedIcon"
                    title="features:password-protection.page.feature-teaser-row.access.title"
                    description="features:password-protection.page.feature-teaser-row.access.description"
                    backgroundColor="Gray"
                    textAlign="Center" />
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="FingerPrintIcon"
                    title="features:password-protection.page.feature-teaser-row.individual-password.title"
                    description="features:password-protection.page.feature-teaser-row.individual-password.description"
                    backgroundColor="Gray"
                    textAlign="Center" />
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
