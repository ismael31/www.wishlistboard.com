import { useRouter } from 'next/router';
import {
    Blockquote,
    Button,
    FeatureTeaser,
    FeatureTeaserRow,
    Section,
    PageHeader
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';

export default function Unlimited() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:unlimited.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:unlimited.page.header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:unlimited.page.header.title"
                subTitle="features:unlimited.page.header.sub-title"
                text="features:unlimited.page.header.text"
            />

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="TrendingUpIcon"
                    title="features:unlimited.page.feature-teaser-row.unlimited.title"
                    description="features:unlimited.page.feature-teaser-row.unlimited.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Blockquote text="features:unlimited.page.blockquote.text" />

           <FeatureTeaserRow title="features:unlimited.page.feature-teaser-row.title">
                <FeatureTeaser
                    icon="UserIcon"
                    title="features:unlimited.page.feature-teaser-row.for-yourself.title"
                    description="features:unlimited.page.feature-teaser-row.for-yourself.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="ClipboardListIcon"
                    title="features:unlimited.page.feature-teaser-row.as-note.title"
                    description="features:unlimited.page.feature-teaser-row.as-note.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="ChatIcon"
                    title="features:unlimited.page.feature-teaser-row.for-others.title"
                    description="features:unlimited.page.feature-teaser-row.for-others.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
