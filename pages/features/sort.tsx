import { useRouter } from 'next/router';
import {
    Button,
    FeatureTeaser,
    FeatureTeaserImage,
    FeatureTeaserRow,
    PageHeader,
    Paragraph,
    Section
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';


export default function Sort() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:sort.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:sort.page.header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:sort.page.header.title"
                subTitle="features:sort.page.header.sub-title"
                text="features:sort.page.header.text"
            />

            <FeatureTeaserImage title="features:sort.page.feature-teaser-image.title"
                                text="features:sort.page.feature-teaser-image.text"
                                image={ {
                                    alternativeText: 'features:sort.page.feature-teaser-image.image.alt',
                                    formats: {
                                        thumbnail: {
                                            url: '/images/wishlist-256x192.png', width: 256,
                                            height: 192
                                        },
                                        small: {
                                            url: '/images/wishlist-512x384.png', width: 512,
                                            height: 384
                                        },
                                        medium: {
                                            url: '/images/wishlist-1024x768.png', width: 1024,
                                            height: 768
                                        },
                                        full: {
                                            url: '/images/wishlist-2048x1536.png', width: 2048,
                                            height: 1536
                                        }
                                    }
                                } } button={ {
                label: 'pages:home.call-to-action.button.label',
                href: 'http://app.wishlistboard.com/register'
            } }/>

            <Paragraph text="features:sort.page.paragraph.text"/>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="AdjustmentsIcon"
                    title="features:sort.page.feature-teaser-row.filter.title"
                    description="features:sort.page.feature-teaser-row.filter.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
