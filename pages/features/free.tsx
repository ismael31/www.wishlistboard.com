import { useRouter } from 'next/router';
import {
    Button, FeatureTeaser,
    FeatureTeaserRow,
    Section,
    PageHeader
} from '../../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../../i18n/i18n';


export default function Free() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('features:free.page.header.title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('features:free.page.header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>

            <PageHeader
                title="features:free.page.header.title"
                subTitle="features:free.page.header.sub-title"
                text="features:free.page.header.text"
            />

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="ThumbUpIcon"
                    title="features:free.page.feature-teaser-row.free.title"
                    description="features:free.page.feature-teaser-row.free.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Section>
                <Button icon={ { left: 'ArrowNarrowLeftIcon' } } label="global:all-features"
                        href={`/${ router.locale }/features`}
                        variation="Tertiary">
                    { t('global:all-features') }
                </Button>
            </Section>
        </>
    );
}
