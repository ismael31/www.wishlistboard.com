import { useRouter } from 'next/router';
import {
    FeatureTeaser,
    FeatureTeaserRow,
    PageHeader,
} from '../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../i18n/i18n';


export default function Impressum() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('pages:impressum.html-title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('pages:impressum.page-header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            {/*for space top*/ }
            <div></div>

            <PageHeader
                title="pages:impressum.page-header.title"
                subTitle="pages:impressum.page-header.sub-title"
                text="pages:impressum.page-header.text"
            />

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="IdentificationIcon"
                    title="pages:impressum.publisher.title"
                    description="pages:impressum.publisher.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

        </>
    );
}
