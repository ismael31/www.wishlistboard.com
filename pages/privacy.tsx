import { useRouter } from 'next/router';
import {
    FeatureTeaser,
    FeatureTeaserRow,
    PageHeader, Paragraph
} from '../components';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../i18n/i18n';


export default function Privacy() {
    const router = useRouter();
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('pages:privacy.html-title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('pages:privacy.page-header.text') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            {/*for space top*/ }
            <div></div>

            <PageHeader
                title="pages:privacy.page-header.title"
                subTitle="pages:privacy.page-header.sub-title"
                text="pages:privacy.page-header.text"
            />

            <Paragraph text="pages:privacy.intro.text"/>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="IdentificationIcon"
                    title="pages:impressum.publisher.title"
                    description="pages:privacy.publisher.description"
                    backgroundColor="Gray"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="DatabaseIcon"
                    title="pages:impressum.data.title"
                    description="pages:impressum.data.description"
                    backgroundColor="Gray"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow>
                <FeatureTeaser
                    icon="DocumentTextIcon"
                    title="pages:impressum.direct-usage.title"
                    description="pages:impressum.direct-usage.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="ChartPieIcon"
                    title="pages:impressum.google-analytics.title"
                    description="pages:impressum.google-analytics.description"
                    label="pages:impressum.google-analytics.label"
                    href="https://policies.google.com/privacy?hl=de"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="ShieldCheckIcon"
                    title="pages:impressum.re-captcha.title"
                    description="pages:impressum.re-captcha.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="StatusOnlineIcon"
                    title="pages:impressum.google.title"
                    description="pages:impressum.google.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="MailOpenIcon"
                    title="pages:impressum.newsletter.title"
                    description="pages:impressum.newsletter.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>

                <FeatureTeaser
                    icon="CashIcon"
                    title="pages:impressum.awin.title"
                    description="pages:impressum.awin.description"
                    label="pages:impressum.awin.label"
                    href="https://www.awin.com/ch/datenschutzerklarung"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <FeatureTeaserRow cols="1">
                <FeatureTeaser
                    icon="DatabaseIcon"
                    title="pages:impressum.dsgvo.title"
                    description="pages:impressum.dsgvo.description"
                    backgroundColor="Gray"
                    textAlign="Center"/>
            </FeatureTeaserRow>

            <Paragraph text="pages:impressum.changes.text"/>

        </>
    );
}
