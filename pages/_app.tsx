import { AppProps } from 'next/app';
import { DefaultSeo } from 'next-seo';
import '../styles/style.css';
import '../styles/tailwind.css';
import { Footer, Header, Navigation } from '../components';
import { FOOTER_NAVIGATION, HEADER_NAVIGATION } from '../data';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import i18next from 'i18next';


export default function MyApp({ Component, pageProps }: AppProps) {
    const router = useRouter();
    const { t } = useTranslation();

    useEffect(() => {
        i18next.changeLanguage(router.locale);
    }, []);

    return (<>
        <DefaultSeo
            titleTemplate="wishlistboard.com | %s"
            description={ t('pages:home.html-title') }
            openGraph={ {} }
            additionalLinkTags={ [
                {
                    rel: 'icon',
                    href: '/favicon.ico'
                }
            ] }
        />

        <Header>
            <Navigation items={ HEADER_NAVIGATION }/>
        </Header>

        <div className="space-y-8 sm:space-y-12 md:space-y-16">
            <Component { ...pageProps } />
        </div>

        <Footer items={ FOOTER_NAVIGATION }/>
    </>);
}
