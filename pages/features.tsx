import { Blockquote, FeatureTeaser, FeatureTeaserRow, PageHeader } from '../components';
import React from 'react';
import { NextSeo } from 'next-seo';
import { FEATURES } from '../data';
import { useTranslation } from 'next-i18next';
import '../i18n/i18n';


const features1 = [FEATURES.CSV, FEATURES.PASSWORD_PROTECTION, FEATURES.PARTS, FEATURES.FREE];
const features2 = [FEATURES.UNLIMITED, FEATURES.SORT, FEATURES.INDEPENDENT];

export default function Slug() {
    const { t } = useTranslation();

    return (<>
        <NextSeo
            title={ t('pages:features.page-header.title') }
            titleTemplate="wishlistboard.com | %s"
            description={ t('pages:features.page-header.text') }
            openGraph={ {} }
            additionalLinkTags={ [
                {
                    rel: 'icon',
                    href: '/favicon.ico'
                }
            ] }
        />

        {/*for space top*/ }
        <div></div>

        <PageHeader
            title="pages:features.page-header.title"
            subTitle="pages:features.title"
            text="pages:features.page-header.text"
        />

        <FeatureTeaserRow>
            { features1.map(({ ...feature }, index) => <FeatureTeaser
                key={ index } { ...feature } />) }
        </FeatureTeaserRow>

        <Blockquote title="pages:features.rezession:title" text="pages:features.rezession:text"/>

        <FeatureTeaserRow>
            { features2.map(({ ...feature }, index) => <FeatureTeaser
                key={ index } { ...feature } />) }
        </FeatureTeaserRow>
    </>);
}
