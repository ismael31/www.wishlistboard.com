import React from 'react';
import { useTranslation } from 'next-i18next';
import { NextSeo } from 'next-seo';
import '../i18n';
import {
    CallToAction,
    CallToActionRow, ContactForm,
    FeatureImages, FeatureTags,
    FeatureTeaser,
    FeatureTeaserRow,
    PageTeaserTitle
} from '../components';
import { FEATURES } from '../data';
import { PricingGrid } from '../components/organisms';
import { PricingItem, PricingItemProps } from '../components/molecules';


const features1 = [FEATURES.CSV, FEATURES.PASSWORD_PROTECTION, FEATURES.PARTS];
const features2 = [FEATURES.FREE, FEATURES.SORT, FEATURES.INDEPENDENT];

const featureImages = [{
    alternativeText: 'features:sort.page.feature-teaser-image.image.alt',
    formats: {
        thumbnail: {
            url: '/images/wishlist-256x192.png', width: 256,
            height: 192
        },
        small: {
            url: '/images/wishlist-512x384.png', width: 512,
            height: 384
        },
        medium: {
            url: '/images/wishlist-1024x768.png', width: 1024,
            height: 768
        },
        full: {
            url: '/images/wishlist-2048x1536.png', width: 2048,
            height: 1536
        }
    }
}, {
    alternativeText: 'features:independent.page.feature-teaser-image.image.alt',
    formats: {
        thumbnail: {
            url: '/images/create-wish-256x192.png', width: 256,
            height: 192
        },
        small: {
            url: '/images/create-wish-512x384.png', width: 512,
            height: 384
        },
        medium: {
            url: '/images/create-wish-1024x768.png', width: 1024,
            height: 768
        },
        full: {
            url: '/images/create-wish-2048x1536.png', width: 2048,
            height: 1536
        }
    }
}, {
    alternativeText: 'features:csv.page.feature-teaser-image.image.alt',
    formats: {
        thumbnail: {
            url: '/images/donates-256x258.png', width: 256, height: 258
        },
        small: {
            url: '/images/donates-512x516.png', width: 512, height: 516
        },
        medium: {
            url: '/images/donates-1024x1032.png', width: 1024, height: 1032
        },
        full: {
            url: '/images/donates-2048x2064.png', width: 2048, height: 2064
        }
    }
}, {
    alternativeText: 'features:password-protection.page.feature-teaser-image.image.alt',
    formats: {
        thumbnail: {
            url: '/images/password-protection-256x192.png', width: 256,
            height: 192
        },
        small: {
            url: '/images/password-protection-512x384.png', width: 512,
            height: 384
        },
        medium: {
            url: '/images/password-protection-1024x768.png', width: 1024,
            height: 768
        },
        full: {
            url: '/images/password-protection-2048x1536.png', width: 2048,
            height: 1536
        }
    }
}, {
    alternativeText: 'features:parts.page.feature-teaser-image.image.alt',
    formats: {
        thumbnail: {
            url: '/images/fulfill-part-256x192.png', width: 256,
            height: 192
        },
        small: {
            url: '/images/fulfill-part-512x384.png', width: 512,
            height: 384
        },
        medium: {
            url: '/images/fulfill-part-1024x768.png', width: 1024,
            height: 768
        },
        full: {
            url: '/images/fulfill-part-2048x1536.png', width: 2048,
            height: 1536
        }
    }
}];

const pricings: PricingItemProps[] = [{
    backgroundColor: 'Bordered',
    title: 'pricing:bronze.title',
    currency: 'CHF',
    amount: 0,
    interval: '',
    description: 'pricing:bronze.description',
    features: ['pricing:bronze.features.scope', 'pricing:bronze.features.advertisement'],
    button: 'pricing:bronze.button'
}, {
    backgroundColor: 'Gray',
    title: 'pricing:gold.title',
    currency: 'CHF',
    amount: 20,
    interval: 'pricing:gold.interval',
    description: 'pricing:gold.description',
    features: ['pricing:gold.features.scope', 'pricing:gold.features.advertisement'],
    button: 'pricing:bronze.button'
}];

export default function Home() {
    const { t } = useTranslation();


    return (<>
            <NextSeo
                title={ t('pages:home.html-title') }
                titleTemplate="wishlistboard.com | %s"
                description={ t('pages:home.html-title') }
                openGraph={ {} }
                additionalLinkTags={ [
                    {
                        rel: 'icon',
                        href: '/favicon.ico'
                    }
                ] }
            />

            {/*for space top*/ }
            <div></div>

            <PageTeaserTitle
                title="pages:home.page-teaser-title.title"
                subTitle="pages:home.page-teaser-title.sub-title"
                button={ {
                    label: 'pages:home.page-teaser-title.button.label',
                    variation: 'Primary',
                    url: 'https://app.wishlistboard.com/register'
                } }
            />

            <FeatureTeaserRow>
                { features1.map(({ ...feature }, index) => <FeatureTeaser
                    key={ index } { ...feature } />) }
            </FeatureTeaserRow>

            <FeatureImages size="Sm" images={ featureImages }/>

            <CallToActionRow>
                <CallToAction
                    title="pages:home.call-to-action.title"
                    text="pages:home.call-to-action.text"
                    button={ {
                        label: 'pages:home.call-to-action.button.label',
                        href: 'https://app.wishlistboard.com/register'
                    } }
                    box={ {
                        backgroundColor: 'Gray'
                    } }/>
            </CallToActionRow>

            <PricingGrid>
                { pricings.map((pricing, index) => <PricingItem key={ index } { ...pricing } />) }
            </PricingGrid>

            <FeatureTags
                icon="QuestionMarkCircleIcon"
                title="pages:home.feature-tags.title"
                text="pages:home.feature-tags.text"
                tags={ ['pages:home.feature-tags.tags.weddings',
                    'pages:home.feature-tags.tags.birthdays',
                    'pages:home.feature-tags.tags.child-birthdays',
                    'pages:home.feature-tags.tags.baby-party',
                    'pages:home.feature-tags.tags.christmas',
                    'pages:home.feature-tags.tags.communion',
                    'pages:home.feature-tags.tags.confirmation',
                    'pages:home.feature-tags.tags.baptism', 'pages:home.feature-tags.tags.etc'] }
            />

            <FeatureTeaserRow>
                { features2.map(({ ...feature }, index) => <FeatureTeaser
                    key={ index } { ...feature } />) }
            </FeatureTeaserRow>

            <ContactForm
                title="pages:contact.sub-title"
                text="pages:contact.text"
                button={ {
                    href: 'https://app.wishlistboard.com/contact',
                    label: 'contact:button.label',
                    passHref: true
                } }
            />

            <CallToActionRow>
                <CallToAction
                    title="pages:home.call-to-action.title"
                    text="pages:home.call-to-action.text"
                    button={ {
                        label: 'pages:home.call-to-action.button.label',
                        href: 'https://app.wishlistboard.com/register'
                    } }
                    box={ {
                        backgroundColor: 'Gray'
                    } }
                />
            </CallToActionRow>
        </>
    );
}
