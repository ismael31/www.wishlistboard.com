import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import LanguageDetector from 'i18next-browser-languagedetector';


const DE = require('../public/locales/de.json');
const EN = require('../public/locales/en.json');

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        lng: 'de',
        fallbackLng: 'en',
        debug: true,
        interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
        },
        resources: {
            en: EN,
            de: DE,
        },
    });


export default i18n;
