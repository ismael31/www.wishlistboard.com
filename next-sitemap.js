module.exports = {
    siteUrl: 'https://wishlistboard.com',
    changefreq: 'Monthly',
    generateRobotsTxt: true,
    alternateRefs: [
        {
            href: 'https://wishlistboard.com/de',
            hreflang: 'de'
        },
        {
            href: 'https://wishlistboard.com/en',
            hreflang: 'en'
        }
    ]
};
