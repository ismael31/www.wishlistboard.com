import { NavigationItemProps } from "../components";

export const HEADER_NAVIGATION: NavigationItemProps[] = [{
    title: 'pages:features.title',
    slug: '/features',
    icon: 'SparklesIcon'
}, {
    title: 'pages:contact.title',
    slug: 'https://app.wishlistboard.com/contact',
    icon: 'ChatIcon',
    passHref: true
}, {
    title: 'pages:login.title',
    slug: 'https://app.wishlistboard.com/login',
    icon: 'LockOpenIcon',
    passHref: true
}, {
    title: 'pages:register.title',
    slug: 'https://app.wishlistboard.com/register',
    isPrimary: true,
    icon: 'UserAddIcon'
}]

export const FOOTER_NAVIGATION: NavigationItemProps[] = [{
    title: 'pages:impressum.html-title',
    slug: '/impressum',
    icon: 'DownloadIcon'
}, {
    title: 'pages:privacy.html-title',
    slug: '/privacy',
    icon: 'DownloadIcon'
}, {
    title: 'pages:contact.title',
    slug: 'https://app.wishlistboard.com/contact',
    icon: 'DownloadIcon',
    passHref: true
}];
