import { FeatureTeaserProps } from '../components/content';


export const FEATURES: { [key: string]: FeatureTeaserProps } = {
    CSV: {
        title: 'features:csv.teaser.title',
        description: 'features:csv.teaser.text',
        icon: 'DownloadIcon',
        href: '/features/export',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    PASSWORD_PROTECTION: {
        title: 'features:password-protection.teaser.title',
        description: 'features:password-protection.teaser.text',
        icon: 'LockClosedIcon',
        href: '/features/password-protection',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    INDEPENDENT: {
        title: 'features:independent.teaser.title',
        description: 'features:independent.teaser.text',
        icon: 'LinkIcon',
        href: '/features/independent',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    UNLIMITED: {
        title: 'features:unlimited.teaser.title',
        description: 'features:unlimited.teaser.text',
        icon: 'TrendingUpIcon',
        href: '/features/unlimited',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    SORT: {
        title: 'features:sort.teaser.title',
        description: 'features:sort.teaser.text',
        icon: 'AdjustmentsIcon',
        href: '/features/sort',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    PARTS: {
        title: 'features:parts.teaser.title',
        description: 'features:parts.teaser.text',
        icon: 'ChartPieIcon',
        href: '/features/parts',
        label: 'global:more',
        backgroundColor: 'Gray'
    },
    FREE: {
        title: 'features:free.teaser.title',
        description: 'features:free.teaser.text',
        icon: 'CurrencyDollarIcon',
        href: '/features/free',
        label: 'global:more',
        backgroundColor: 'Gray'
    }
};
