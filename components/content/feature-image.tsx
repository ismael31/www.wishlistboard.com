import Image from 'next/image'
import { ImageType } from "../../types";

export type FeatureImageFormats = { thumbnail: ImageType, small: ImageType, medium: ImageType, full: ImageType };

export type FeatureImageProps = {
    alternativeText: string;
    onClick?: (image: FeatureImageProps) => void;
    format: ImageType;
};

export function FeatureImage(image: FeatureImageProps) {
    const { alternativeText, onClick, format: { url, width, height } } = image;
    const border = onClick && 'border border-gray-200 p-6 hover:border-gray-300' +
        ' focus:border-gray-300';

    return (
        <div
            className={ `inline-block cursor-pointer flex-shrink-0 flex items-center ${ border }` }>
            <Image src={ `${ url }` } alt={ alternativeText } width={ width }
                   height={ height }
                   onClick={ () => onClick && onClick(image) }/>
        </div>
    );
}


