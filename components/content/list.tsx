import { Box, BoxProps, H2, ListElement, Ol, Section, Ul } from '../elements';

enum ListType {
    ul = 'ul',
    ol = 'ol'
}

export type ListProps = {
    title?: string;
    elements: ListElement[];
    type: ListType;
    box: Partial<BoxProps>;
};

export function List({ title, elements, type, box }: ListProps) {
    return (
        <Section>
            <Box { ...box } pagewidth="Text">
                <div className="space-y-4">
                    <div>sfsfsfsf</div>
                    { title && <H2>{ title }</H2> }
                    { type === ListType.ol && <Ol elements={ elements }/> }
                    { type === ListType.ul && <Ul elements={ elements }/> }
                </div>
            </Box>
        </Section>
    );
}


