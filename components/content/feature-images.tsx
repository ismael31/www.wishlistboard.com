import { Button, Icon, Section } from '../elements';
import { FeatureImage, FeatureImageFormats } from './feature-image';
import { useEffect, useState } from 'react';
import { Size } from '../../types';


const getImage = (formats: FeatureImageFormats, size: Size) => {
    if (size === 'Sm') {
        return formats.thumbnail;
    }

    if (size === 'Md') {
        return formats.medium;
    }

    return formats.full;
};

export type FeatureImageType = {
    alternativeText: string;
    formats: FeatureImageFormats;
}

export type FeatureImagesProps = {
    images: FeatureImageType[];
    size: Size;
};

export function FeatureImages({ images: imgs, size }: FeatureImagesProps) {
    const images: (FeatureImageType & { index: number })[] = imgs.map((img, index) => ({
        ...img, index
    }));
    const [active, setActive] = useState<FeatureImageType & { index: number } | null>(null);

    function handleKeyPress(event) {
        if (event.keyCode === 27) {
            setActive(null);
        }
    }

    useEffect(() => {
        window.addEventListener('keyup', handleKeyPress);

        return () => {
            window.removeEventListener('keyup', handleKeyPress);
        };
    }, []);

    return (
        <Section>
            { active && <>
                <div
                    className="fixed inset-0 bg-gray-900 bg-opacity-80 z-50 flex items-center justify-center p-8">
                    <div className="absolute z-50 right-4 top-4">
                        <Button variation="Lightbox" label="" onClick={ () => setActive(null) }>
                            <Icon name="XIcon"/>
                        </Button>
                    </div>

                    { active && active.index < (images.length - 1) &&
                    <div className="absolute z-50 right-4 top-50">
                        <Button variation="Lightbox" label=""
                                onClick={ () => setActive(images[active.index + 1])
                                }>
                            <Icon name="ArrowNarrowRightIcon"/>
                        </Button>
                    </div>
                    }

                    { active && active.index > 0 &&
                    <div className="absolute z-50 left-4 top-50">
                        <Button variation="Lightbox" label=""
                                onClick={ () => setActive(images[active.index - 1]) }>
                            <Icon name="ArrowNarrowLeftIcon"/>
                        </Button>
                    </div>
                    }

                    <div className="max-h-100 max-w-100">
                        <FeatureImage alternativeText={ active.alternativeText }
                                      format={ active.formats.full }/>
                    </div>
                </div>
            </>
            }

            <div className="flex space-x-4 overflow-x-auto pb-4">
                { images.map((image, index) => {
                        const format = getImage(image.formats, size);
                        const { alternativeText } = image;

                        return <FeatureImage key={ index }
                                             alternativeText={ alternativeText }
                                             format={ format }
                                             onClick={ () => setActive(image) }/>;
                    }
                ) }
            </div>
        </Section>
    );
}


