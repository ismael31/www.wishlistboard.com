import Image from 'next/image';
import { Button, H1, P, Section } from '../elements';
import { ButtonType } from '../../types';
import { useTranslation } from 'next-i18next';
import { FeatureImage } from './feature-image';
import { useRouter } from 'next/router';


export type PageTeaserTitleProps = {
    title: string;
    subTitle: string;
    button?: ButtonType;
};

export function PageTeaserTitle({ title, subTitle, button }: PageTeaserTitleProps) {
    const { t } = useTranslation();
    const router = useRouter();
    const { url, ...buttonProps } = button;

    return (
        <Section>
            <div className="sm:flex sm:items-center">
                <div className="block sm:hidden">
                    <Image src="/icon.png" alt="wishlistboard.com Logo" width={ 148.8 }
                           height={ 93 }/>
                </div>

                <div className="flex-grow mr-4 space-y-1">
                    <H1>{ t(title) }</H1>
                    <P textColor="Gray" text={ t(subTitle) }/>

                    { button && <div className="pt-2">
                        <Button { ...buttonProps }
                                href={ url }>{ t(button.label) }</Button>
                    </div> }
                </div>

                <div className="flex-shrink-0 ml-4 hidden sm:block">
                    <FeatureImage alternativeText="wishlistboard.com Logo" format={ {
                        url: '/icon.png', width: 297, height: 186
                    } }/>
                </div>
            </div>
        </Section>
    );
}


