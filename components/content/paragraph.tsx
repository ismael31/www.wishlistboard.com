import { Box, H3, Ol, Section, Ul } from '../elements';
import Markdown from 'react-markdown';
import { useTranslation } from 'next-i18next';
import { Fragment } from 'react';


export const markdown = {
    strong: ({ children }) => {
        return children.map(({ props }, index) => {
            return <strong key={ index } className="text-bold">{ props.children }</strong>;
        });

    },
    heading: ({ children }) => {
        return children.map(({ props }, index) => {
            return <H3 key={ index }>{ props.children }</H3>;
        });

    },
    list: (list) => {
        const elements = list.children.map(({ props }, index) => {
            return { key: index, text: props.children[0].props.value };
        });

        if (list.node.ordered) {
            return <Ol elements={ elements }/>;
        }

        return <Ul elements={ elements }/>;
    },
    paragraph: ({ children }) => {
        return <div className="space-y-4">{ children.map(({ props }, index) => {
            return <p key={ index }
                      className="text-base">{ (props.value ? props.value.split('[br]') : [])
                .map((text, index) => <Fragment key={ index }>{ text }<br/></Fragment>) }</p>;
        }) }</div>;
    }
};

export type ParagraphProps = {
    text: string;
};

export function Paragraph({ text }: ParagraphProps) {
    const { t } = useTranslation();

    return (
        <Section>
            <Box pagewidth="Text">
                <div className="space-y-4">
                    { <Markdown source={ t(text) } renderers={ markdown }/> }
                </div>
            </Box>
        </Section>
    );
}


