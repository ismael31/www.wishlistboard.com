import { Box, Button, H3, Icon, IconSize, P } from '../elements';
import { BackgroundColor, TEXT_ALIGNS, TextAlign } from '../../types';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';


export type FeatureTeaserProps = {
    title: string;
    description: string;
    icon?: string;
    href?: string;
    label?: string;
    backgroundColor?: BackgroundColor;
    textAlign?: TextAlign;
};

export function FeatureTeaser({ title, description, icon, href, label, backgroundColor = 'Transparent', textAlign = 'Left' }: FeatureTeaserProps) {
    const { t } = useTranslation();
    const router = useRouter();

    return (
        <Box border={ backgroundColor === 'Transparent' ? 'Gray' : 'None' }
             backgroundColor={ backgroundColor }>
            <div className={ `space-y-2 flex-grow ${ TEXT_ALIGNS[textAlign] }` }>
                { icon &&
                <div className={ `text-carnation-500 ${ textAlign ===
                'Center' ? 'flex justify-center' : '' }` }>
                    <Icon name={ icon } size={ IconSize.Xl }/>
                </div> }
                <H3>{ t(title) }</H3>
                <P text={ t(description) }/>
            </div>

            { href && label &&
            <div className="mt-6 flex justify-end items-center">
                <Button variation="Tertiary" label={ label }
                        href={ `/${ router.locale }${ href }` }>
                    <span>{ t(label) }</span>
                    <Icon name="ArrowNarrowRightIcon"/>
                </Button>
            </div> }
        </Box>
    );
}


