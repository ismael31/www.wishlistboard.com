import { Button, Icon } from '../elements';
import { FeatureImage } from './feature-image';
import { useEffect } from 'react';
import { ImageType } from '../../types';


export type FeatureImageOverlayProps = {
    alternativeText: string;
    image: ImageType;
    onClose: () => void;
};

export function FeatureImageOverlay({ alternativeText, image, onClose }: FeatureImageOverlayProps) {

    function handleKeyPress(event) {
        if (event.keyCode === 27) {
            onClose();
        }
    }

    useEffect(() => {
        window.addEventListener('keyup', handleKeyPress);

        return () => {
            window.removeEventListener('keyup', handleKeyPress);
        };
    }, []);

    return (
        <>
            <div
                className="fixed inset-0 bg-gray-900 bg-opacity-80 z-50 flex items-center justify-center p-8">
                <div className="absolute z-50 right-4 top-4">
                    <Button variation="Lightbox" label="" onClick={ () => onClose() }>
                        <Icon name="XIcon"/>
                    </Button>
                </div>

                <div className="max-h-100 max-w-100">
                    <FeatureImage alternativeText={ alternativeText }
                                  format={ image }/>
                </div>
            </div>
        </>
    );
}


