import { Section } from '../elements';
import { FC } from 'react';


export type CallToActionRowProps = {};

export const CallToActionRow: FC<CallToActionRowProps> = ({ children }) => {
    return (
        <Section>
            <div
                className="flex flex-col space-y-4 md:flex-row md:space-x-6 md:space-y-0">{ children }</div>
        </Section>
    );
};


