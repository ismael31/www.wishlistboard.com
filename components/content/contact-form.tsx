import {
    Box,
    Button,
    ButtonProps,
    Section,
    SectionProps
} from '../elements';
import { useTranslation } from 'next-i18next';
import { markdown } from './paragraph';
import Markdown from 'react-markdown';


export type ContactFormProps = {
    title?: string;
    text?: string;
    section?: Partial<SectionProps>;
    button: Omit<ButtonProps, 'children'>;
};

export function ContactForm({ title, text, section, button }: ContactFormProps) {
    const { t } = useTranslation();

    return (
        <Section { ...section } pagewidth="Text" textAlign="Center" title={ title }>
            <Box textAlign="Center">
                <div className="space-y-6">
                    { text && <Markdown source={ t(text) } renderers={ markdown }/> }
                    <div className="mx-auto">
                        <Button { ...button } type="button"
                                variation="Tertiary"
                                icon={ { right: 'ArrowNarrowRightIcon' } }>{ t(button.label) }</Button>
                    </div>
                </div>
            </Box>
        </Section>
    );
}


