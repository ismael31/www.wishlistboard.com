import { Box, Section } from '../elements';
import { useTranslation } from 'next-i18next';

import { Small } from '../elements';


export type BlockquoteProps = {
    title?: string;
    text: string;
};

export function Blockquote({ title, text }: BlockquoteProps) {
    const { t } = useTranslation();

    return (
        <Section>
            <Box pagewidth="Text" textAlign="Center">
                <div className="space-y-4">
                    <blockquote
                        className='text-2xl font-serif font-bold text-gray-700 text-center'>{
                        t(text) }</blockquote>

                    { title && <Small>{ t(title) }</Small> }
                </div>
            </Box>
        </Section>
    );
}


