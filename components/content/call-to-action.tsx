import { Box, BoxProps, Button, ButtonProps, H3, P } from '../elements';
import { useTranslation } from 'next-i18next';


export type CallToActionProps = {
    title: string;
    text: string;
    button: Omit<ButtonProps, 'children'> & { href: string };
    box: Partial<BoxProps>;
};

export function CallToAction({ title, text, button, box }: CallToActionProps) {
    const { t } = useTranslation();

    return (
        <Box { ...box } backgroundColor="Carnation" textAlign="Center" pagewidth="Text">
            <div className="space-y-4 flex-grow">
                <H3 textColor="CarnationDark">{ t(title) }</H3>
                <P textColor="CarnationDark" text={ t(text) }/>
            </div>

            <div className="mt-6">
                <Button { ...button } href={ button.href }
                        variation="Tertiary" passHref={ button?.passHref }
                        icon={ { right: 'ArrowNarrowRightIcon' } }>{ t(button.label) }</Button>
            </div>
        </Box>
    );
}


