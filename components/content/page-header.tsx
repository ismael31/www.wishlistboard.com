import { Box, H3, H4, Section } from '../elements';
import { useTranslation } from 'next-i18next';


export type PageHeaderProps = {
    subTitle: string;
    title: string;
    text: string;
}

export function PageHeader({ subTitle, title, text }) {
    const { t } = useTranslation();

    return (
        <Section>
            <Box>
                <div className="text-center space-y-1">
                    <H3 textColor="Carnation"><span
                        className="text-lg sm:text-xl">{ t(subTitle) }</span></H3>
                    <p className="text-2xl sm:text-3xl leading-10 font-serif font-bold text-gray-900">{ t(title) }</p>
                    <p className="pt-1 text-lg sm:text-xl font-serif text-gray-500">{ t(text) }</p>
                </div>
            </Box>
        </Section>);
}
