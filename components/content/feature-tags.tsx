import { Badge, Box, H3, Icon, IconSize, P, Section } from '../elements';
import { useTranslation } from 'next-i18next';


export type FeatureTagsProps = {
    title: string;
    text: string;
    icon: string;
    tags: string[];
};

export function FeatureTags({ title, text, icon, tags }: FeatureTagsProps) {
    const { t } = useTranslation();

    return (
        <Section pagewidth="Text">
            <Box>
                <div className="text-carnation-500 mb-2">
                    <Icon name={ icon } size={ IconSize.Xl }/>
                </div>
                <div className="space-y-4">
                    <H3>{ t(title) }</H3>
                    <P text={ t(text) }/>

                    <div className="mt-4 flex flex-col space-y-2">
                        { tags?.map((tag, index) => {
                            return (<span key={ index }><Badge>{ t(tag) }</Badge></span>);
                        }) }
                    </div>
                </div>
            </Box>
        </Section>
    );
}


