import {
    Box,
    Button,
    ButtonProps,
    H3,
    Section,
    SectionProps
} from '../elements';
import { markdown } from './paragraph';
import Markdown from 'react-markdown';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { FeatureImageType } from './feature-images';
import { useState } from 'react';
import { FeatureImageOverlay } from './feature-image-overlay';


export type FeatureTeaserImageProps = {
    section?: Partial<SectionProps>;
    title: string;
    text: string;
    image: FeatureImageType;
    button: Omit<ButtonProps, 'children'>;
};

export function FeatureTeaserImage({ section, title, text, image, button }: FeatureTeaserImageProps) {
    const { t } = useTranslation();

    const [isOpen, setIsOpen] = useState<boolean>(false);

    const { alternativeText, formats: { small, medium, full } } = image;

    return (
        <Section { ...section }>
            <Box backgroundColor="Gray">
                <div className="space-y-4 text-center">
                    <H3>{ t(title) }</H3>

                    <Markdown source={ t(text) } renderers={ markdown }/>

                    <Button { ...button } variation="Tertiary"
                            icon={ { right: 'ArrowNarrowRightIcon' } }>{ t(button.label) }</Button>

                    <div className="text-center hidden md:block">
                        <Image src={ `${ medium.url }` }
                               alt={ t(alternativeText) } width={ medium.width }
                               height={ medium.height }
                               onClick={ () => setIsOpen(true) }/>
                    </div>

                    <div className="text-center md:hidden">
                        <Image src={ `${ small.url }` }
                               alt={ t(alternativeText) } width={ small.width }
                               height={ small.height }
                               onClick={ () => setIsOpen(true) }/>
                    </div>
                </div>
            </Box>

            { isOpen && <FeatureImageOverlay alternativeText={ alternativeText } image={ full }
                                             onClose={ () => setIsOpen(false) }/> }
        </Section>
    );
}


