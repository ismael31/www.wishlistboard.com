import { Section, SectionProps } from '../elements';
import { FC } from 'react';


export type FeatureTeaserRowProps = {
    section?: Partial<SectionProps>;
    title?: string;
    cols?: '1' | '2';
};

export const FeatureTeaserRow: FC<FeatureTeaserRowProps> = ({
    section, title, cols = '2', children
}) => {

    return (
        <Section { ...section } title={ title }>
            <div className={ `grid grid-cols-1 gap-4 sm:gap-6 md:gap-8 ${ cols ===
            '2' ? 'sm:grid-cols-2' : 'sm:grid-cols-1' }` }>
                { children }
            </div>
        </Section>
    );
};


