import Image from 'next/image';
import Link from 'next/link';


export type HeaderProps = {
    children: React.ReactNode;
};

export function Header({ children }: HeaderProps) {
    return (
        <div className="flex items-center justify-between px-8 py-4">
            <div>
                <Link href="/">
                    <a title="wishlistboard.com">
                        <Image src="/wishlistboard.com-logo.png" width="227" height="26"
                               alt="Logo Wishlistboard" className="cursor-pointer"/>
                    </a>
                </Link>
            </div>
            <div>{ children }</div>
        </div>
    );
}


