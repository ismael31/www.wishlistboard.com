import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import { Icon } from './elements';
import { useRouter } from 'next/router';


const BASE_STYLE = 'flex flex-grow justify-center text-center font-medium text-grey-100 cursor-pointer';

const STYLES: Record<string, string> = {
    primary: `${ BASE_STYLE } md:text-white md:bg-carnation-500 md:rounded md:hover:bg-carnation-700`,
    default: `${ BASE_STYLE }`
};

export type NavigationItemProps = {
    title: string;
    slug: string;
    isPrimary?: boolean;
    icon: string;
    passHref?: boolean;
};

export type NavigationProps = {
    items?: NavigationItemProps[];
};

export function Navigation({ items }: NavigationProps) {
    const router = useRouter();
    const { t } = useTranslation();

    return (
        <div
            className="fixed bottom-0 left-0 right-0 bg-white py-1 shadow-xs border-t border-grey-100 md:relative md:bg-transparent md:p-0 md:border-none">
            <ul className="flex items-center space-x-2">
                { items?.map((
                    { title, slug, isPrimary, icon, passHref }: NavigationItemProps,
                    index: number) => {
                    return (
                        <li key={ index } className={ STYLES[isPrimary ? 'primary' : 'default'] }>
                            <Link href={ `/${ router.locale }${ slug }` } passHref={ passHref }>
                                <a title={ t(title) }
                                   className="flex flex-col px-2 md:px-4 py-2 rounded focus:outline-none hover:text-carnation-500 md:focus:bg-gray-200 md:focus:text-gray-900 md:focus:ring-gray-300 md:focus:ring-4 md:focus:ring-opacity-25 transition-all duration-300 outline-none space-y-1 md:space-y-0">
                                    <div
                                        className="text-center text-carnation-500 flex self-center md:hidden">
                                        <Icon name={ icon }/>
                                    </div>
                                    <span
                                        className="block text-sm font-serif md:text-base">{ t(title) }</span>
                                </a>
                            </Link>
                        </li>);
                }) }
            </ul>
        </div>
    );
}


