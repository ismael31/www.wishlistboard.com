import {
    Badge,
    Box, BoxProps,
    Button,
    H2,
    Icon,
    Small
} from '../elements';
import { useTranslation } from 'next-i18next';
import React, { FC } from 'react';
import { BackgroundColor, TEXT_COLORS } from '../../types';


export type PricingItemProps = {
    backgroundColor?: BackgroundColor;
    title: string;
    currency: string;
    amount: number;
    interval?: string;
    description: string;
    features: string[];
    button: string;
};

export const PricingItem: FC<PricingItemProps> = ({
    backgroundColor = 'Gray',
    title,
    currency,
    amount,
    interval,
    description,
    features,
    button
}) => {
    const { t } = useTranslation();

    return (
        <Box backgroundColor={ backgroundColor }>
            <div className="space-y-2">
                <div className="flex">
                    <Badge>{ t(title) }</Badge>
                </div>
                <H2>
                    <span className="flex items-start space-x-1.5">
                        <Small>{ currency }</Small>
                        <span
                            className={ `text-3xl ${ TEXT_COLORS.Carnation }` }>{ amount }</span>
                        { interval &&
                        <span
                            className={ `self-end ${ TEXT_COLORS.Gray }` }>/ { t(interval) }</span> }
                    </span>
                </H2>
            </div>
            <div
                className={ `flex items-end mt-4 text-base h-auto h-16 md:h-16 lg:h-14 ${ TEXT_COLORS.Gray }` }>{ t(description) }</div>

            <div
                className="border-t mt-6 pt-6 space-y-6">
                <ul role="list" className="space-y-4">
                    { features.map((feature, index) => (
                        <li key={ index } className="flex items-start">
                            <div className="flex-shrink-0">
                                <Icon name="CheckCircleIcon"/>
                            </div>
                            <p className="ml-3 text-base text-gray-700">{ t(feature) }</p>
                        </li>
                    )) }
                </ul>
                <div className="w-full text-right">
                    <Button href="https://app.wishlistboard.com/register"
                            icon={ { right: 'ArrowNarrowRightIcon' } }
                            label="Los geht's" variation="Link"
                            block={ true }>{ t(button) }</Button>
                </div>
            </div>
        </Box>
    );
};


