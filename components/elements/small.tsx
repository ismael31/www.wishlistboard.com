import {
    BACKGROUND_COLORS,
    BackgroundColor,
    Border, BORDERS,
    Pagewidth, PAGEWIDTHS,
    TEXT_ALIGNS, TEXT_COLORS,
    TextAlign, TextColor
} from '../../types';


export type SmallProps = {
    textColor?: TextColor;
    children: React.ReactNode;
};

export function Small({ textColor = 'Gray', children }: SmallProps) {

    return (<small className={ `text-sm ${ TEXT_COLORS[textColor] }` }>
        { children }
    </small>);
}
