import { BUTTON_SIZES, BUTTON_VARIATIONS, Size, Variation } from '../../types';
import { useTranslation } from 'next-i18next';
import { Icon } from './icon';


export type ButtonOnClickType = { [key: string]: any };

export type ButtonProps = {
    onClick?: (data?: ButtonOnClickType) => void;
    href?: string;
    label: string;
    type?: 'button' | 'submit';
    variation?: Variation;
    size?: Size;
    disabled?: boolean;
    block?: boolean;
    children: React.ReactNode;
    icon?: {
        left?: string;
        right?: string;
    },
    passHref?: boolean;
};

export function Button({ onClick, href, label, type = 'button', variation = 'Secondary', size = 'Md', disabled = false, block = false, icon, children }: ButtonProps) {
    const { t } = useTranslation();

    const style = `inline-flex space-x-2 font-bold items-center border border-transparent font-medium rounded focus:outline-none disabled:opacity-33 ${ BUTTON_VARIATIONS[variation] } ${ BUTTON_SIZES[size] } ${ block ? 'w-full justify-center sm:w-auto' : '' }`;

    if (href) {
        return <a href={ href } title={ t(label) } className={ style }>
            { icon?.left && <span className="mr-2"><Icon name={ icon.left }/></span> }
            { children }
            { icon?.right && <span className="ml-2"><Icon name={ icon.right }/></span> }
        </a>;
    }

    return (
        <button type={ type } onClick={ () => onClick && onClick() } title={ t(label) }
                className={ style }
                disabled={ disabled }>
            { icon?.left && <span className="mr-2"><Icon name={ icon.left }/></span> }
            { children }
            { icon?.right && <span className="ml-2"><Icon name={ icon.right }/></span> }
        </button>
    );
}
