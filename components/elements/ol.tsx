import Markdown from 'react-markdown';
import { useTranslation } from 'next-i18next';

export type ListElement = {
    id: number;
    text: string;
}

export type OlProps = {
    elements: ListElement[];
};

export function Ol({ elements }: OlProps) {
    const { t } = useTranslation();

    return (
        <ol className="list-decimal list-outside pl-6 space-y-1">
            { elements.map((element, index) => {
                return <li key={ index }>{ <Markdown source={ t(element.text) }
                                                     escapeHtml={ false }/> }</li>;
            }) }
        </ol>
    );
}


