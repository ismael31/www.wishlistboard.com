import {
    BACKGROUND_COLORS,
    BackgroundColor,
    Pagewidth,
    PAGEWIDTHS,
    TEXT_ALIGNS,
    TextAlign
} from '../../types';
import { H2 } from './h';
import { useTranslation } from 'next-i18next';


export type SectionProps = {
    pagewidth?: Pagewidth;
    backgroundColor?: BackgroundColor;
    textAlign?: TextAlign;
    title?: string;
    children: React.ReactNode;
};

export function Section({
    pagewidth = 'Default', backgroundColor = 'Transparent', textAlign = 'Left', title, children
}: SectionProps) {
    const { t } = useTranslation();

    const style = `${ PAGEWIDTHS[pagewidth] } ${ TEXT_ALIGNS[textAlign] } ${ BACKGROUND_COLORS[backgroundColor] } mx-auto`;

    return (
        <section className={ `flex space-x-8 ${ style }` }>
            <div className="mx-auto px-4 md:px-6 lg:px-8 w-full">
                { title && <div className="mb-6"><H2>{ t(title) }</H2></div> }
                { children }
            </div>
        </section>
    );
}
