import { useTranslation } from 'next-i18next';


export type AProps = {
    title: string;
    href: string;
    children: React.ReactNode;
};

export function A({ title, href, children }: AProps) {
    const { t } = useTranslation();

    return (
        <a href={ href } title={ t(title) }
           className="text-carnation-500 font-bold flex items-center space-x-2 hover:underline hover:text-carnation-700">{ children }</a>
    );
}
