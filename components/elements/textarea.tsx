import { inputStyle } from './input';
import { useTranslation } from 'next-i18next';

export type TextareaProps = {
    name: string;
};

export function Textarea({ name }: TextareaProps) {
    const { t } = useTranslation();

    return (
        <textarea id={ name } name={ t(name) } rows={ 4 } className={ inputStyle }/>
    );
}
