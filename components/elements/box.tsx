import {
    BACKGROUND_COLORS,
    BackgroundColor,
    Border, BORDERS,
    Pagewidth, PAGEWIDTHS,
    TEXT_ALIGNS,
    TextAlign
} from '../../types';


export type BoxProps = {
    backgroundColor?: BackgroundColor;
    border?: Border;
    textAlign?: TextAlign;
    pagewidth?: Pagewidth;
    children: React.ReactNode;
};

export function Box({ backgroundColor = 'Transparent', textAlign = 'Left', pagewidth = 'Full', border = 'None', children }: BoxProps) {
    const style = `${ TEXT_ALIGNS[textAlign] } ${ BACKGROUND_COLORS[backgroundColor] } ${ BORDERS[border] } ${ backgroundColor ===
    'Transparent' &&
    border ===
    'None' ? 'px-4 md:px-8 lg:px-12' : 'px-4 md:px-8 lg:px-12 py-6 sm:py-8 md:py-12' } rounded-lg flex-grow`;

    return (<div className={ style }>
        <div
            className={ `${ PAGEWIDTHS[pagewidth] } mx-auto flex flex-col h-full` }>{ children }</div>
    </div>);
}
