import { TEXT_COLORS, TextColor } from '../../types';
import Markdown from 'react-markdown';
import { markdown } from '../content';


export type PProps = {
    textColor?: TextColor;
    text: string;
};

export function P({ text, textColor = 'Black' }: PProps) {
    return (
        <div className={ `font-medium text-base space-y-4 ${ TEXT_COLORS[textColor] }` }>{ <Markdown
            source={ text } renderers={ markdown }/> }</div>
    );
}
