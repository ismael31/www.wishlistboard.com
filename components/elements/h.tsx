import { TEXT_COLORS, TextColor } from '../../types';


export type HProps = {
    textColor?: TextColor;
    children: React.ReactNode;
};

export function H1({ children, textColor = 'Black' }: HProps) {
    return (
        <h1 className={ `text-2xl sm:text-3xl md:text-4xl font-medium font-serif break-words ${ TEXT_COLORS[textColor] }` }>{ children }</h1>
    );
}

export function H2({ children, textColor = 'Black' }: HProps) {
    return (
        <h2 className={ `text-xl sm:text-2xl font-medium font-serif break-words ${ TEXT_COLORS[textColor] }` }>{ children }</h2>
    );
}

export function H3({ children, textColor = 'Black' }: HProps) {
    return (
        <h3 className={ `text-xl sm:text-2xl font-medium font-serif break-words ${ TEXT_COLORS[textColor] }` }>{ children }</h3>
    );
}

export function H4({ children, textColor = 'Black' }: HProps) {
    return (
        <h4 className={ `text-md font-medium break-words ${ TEXT_COLORS[textColor] }` }>{ children }</h4>
    );
}
