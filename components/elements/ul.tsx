import Markdown from 'react-markdown';
import { ListElement } from './ol';
import { useTranslation } from 'next-i18next';


export type UlProps = {
    elements: ListElement[];
};

export function Ul({ elements }: UlProps) {
    const { t } = useTranslation();

    return (
        <ul className="list-disc list-outside pl-6 space-y-1">
            { elements.map((element, index) => {
                return <li key={ index }>{ <Markdown source={ t(element.text) }
                                                     escapeHtml={ false }/> }</li>;
            }) }
        </ul>
    );
}


