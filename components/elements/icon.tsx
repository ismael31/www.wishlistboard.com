import React from 'react';
import * as HeroIcons from '@heroicons/react/outline';


export enum IconSize {
    Xs = 'XS',
    Sm = 'SM',
    Md = 'Md',
    Lg = 'LG',
    Xl = 'XL',
    Xxl = 'XXL',
}

const ICON_SIZES: Record<IconSize, string> = {
    [IconSize.Xs]: 'w-4 h-4',
    [IconSize.Sm]: 'w-6 h-6',
    [IconSize.Md]: 'w-8 h-8',
    [IconSize.Lg]: 'w-12 h-12',
    [IconSize.Xl]: 'w-16 h-16',
    [IconSize.Xxl]: 'w-24 h-24'
};

export type IconProps = {
    name: string;
    size?: IconSize;
};

export function Icon({ name, size = IconSize.Sm }: IconProps) {

    const component = HeroIcons[name];

    if (!component) {
        console.error(`Icon [${ name }] not found!`);
        return null;
    }

    return (
        <span className={ `block ${ ICON_SIZES[size] }` }>{ React.createElement(component) }</span>
    );
}
