
export type BadgeProps = {
    children: React.ReactNode;
};

export function Badge({ children }: BadgeProps) {
    return (
        <span
            className="inline px-3 py-1 rounded-full text-sm font-medium bg-carnation-500 text-carnation-100">{ children }</span>

    );
}
