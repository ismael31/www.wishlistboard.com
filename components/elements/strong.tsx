import {
    BACKGROUND_COLORS,
    BackgroundColor,
    Border, BORDERS,
    Pagewidth, PAGEWIDTHS,
    TEXT_ALIGNS, TEXT_COLORS,
    TextAlign, TextColor
} from '../../types';


export type StrongProps = {
    textColor?: TextColor;
    children: React.ReactNode;
};

export function Strong({ textColor = 'Black', children }: StrongProps) {

    return (<strong className={ `text-bold ${ TEXT_COLORS[textColor] }` }>
        { children }
    </strong>);
}
