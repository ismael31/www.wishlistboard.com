import { useTranslation } from 'next-i18next';


export type InputType = 'text' | 'email';

export const inputStyle = 'py-2 px-4 block w-full shadow-sm border border-gray-300 focus:border-carnation-700 focus:outline-none rounded';

export type InputProps = {
    name: string;
    type: InputType;
};

export function Input({ name, type }: InputProps) {
    const { t } = useTranslation();

    return (
        <input id={ name } name={ t(name) } type={ type } className={ inputStyle }/>
    );
}
