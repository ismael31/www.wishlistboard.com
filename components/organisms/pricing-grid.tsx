import { Section } from '../elements';
import { FC } from 'react';


export const PricingGrid: FC = ({ children }) => {
    return (
        <Section>
            <div
                className="grid grid-cols-1 gap-4 sm:gap-6 md:gap-8 md:grid-cols-2">{ children }</div>
        </Section>
    );
};


